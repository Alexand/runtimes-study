# Runtimes Study

Purpose of this repo is to learn and document the existing solutions for implementing Serverless Runtimes.

## Index

* [Terms and concepts](#identifying-terms-and-concepts)
* [Exploring project riff solution with CN Buildpacks](#exploring-project-riff-solution-with-cn-buildpacks)
* [Plans for future runtimes](#plans-for-future-runtimes)
* [Study on how to inject dependencies](#study-on-how-to-inject-dependencies)

## Terms and concepts

### Runtime

It's basically an image that comprehends a function invoker and all the OS and project dependencies needed to run a function.

### Function invoker

Usually it's an http server that get function calls via request and forwards a request context and arguments to the function being invoked.

### Buildpacks

This buildpacks reference [API v2a Heroku](https://devcenter.heroku.com/articles/buildpack-api) and [API v2b Cloud Foundry](https://docs.cloudfoundry.org/buildpacks/)

Exist since 2011. They were conceived by Heroku and later adopted by Cloud Foundry and other PaaS. These buildpacks were not made to build an [OCI image](https://github.com/opencontainers/image-spec). Their goal was to build and deploy applications from their source code on the their PaaS. The buildpacks should take all the infrastructure and deployment config burden from the developer, so it only needs to worry about the application source code. It also seems like these buildpacks were usually expecting some kind of complete web service source code to be able to deploy it, thus making it unfit for functions.

##### Available API v2 buildpacks
Currently there are over 5000 [api v2 buildpacks](https://elements.heroku.com/buildpacks)

### CN Buildpacks

This buildpacks reference [Buildpack API v3 specification](https://github.com/buildpack/spec)

Exists since 2018 when Heroku and Cloud Foundry decided to join their knowledge of years of work to define a specification standard for creating buildpacks. The objectives of this buildpacks as per [https://buildpacks.io/](https://buildpacks.io/) and [Buildpack API v3 specification](https://github.com/buildpack/spec) are:

*  reduce the operational burden on developers and support enterprise operators who manage apps at scale.
*  embrace modern container standards, such as the OCI image format. They take advantage of the latest capabilities of these standards, such as cross-repository blob mounting and [image layer "rebasing"](https://buildpacks.io/docs/using-pack/update-app-rebase/) on Docker API v2 registries
*  provide a higher-level abstraction for building apps compared to Dockerfiles
*  automated delivery of both OS-level and application-level dependency upgrades, efficiently handling day-2 app operations that are often difficult to manage with Dockerfiles.
*  rely on compatibility guarantees to safely apply patches without rebuilding artifacts and without unintentionally changing application behavior.

##### Available API v3 buildpacks
As of today, Cloud Native has implemented [sample buildpacks for NodeJS and Java](https://github.com/buildpack/samples). They're still porting other API v2 buildpacks into API v3 buildpacks

It is also expected that the community start adhering to API v3 and contribute with their own customized buildpacks. 

[Projectriff has created also their own buildpacks](https://github.com/projectriff?utf8=%E2%9C%93&q=buildpack&type=&language=) for NodeJS, Java, commands and riff related logic.

### Buildpacks v3 Lifecycle

It's an [implementation of CN Buildpacks API v3 lifecycle component](https://github.com/buildpack/lifecycle)

### CN Buildpack CLI (`pack`) 

It's a [CLI for building apps using CN Buildpacks](https://github.com/buildpack/pack)

That being said, CN Buildpacks (API v3) is trying to define standars upon which buildpacks should be created to guarantee more security, faster build times, less operational burden on developer, etc, and it's being created by 2 companies that have great knowledge around this business. Lincense is Apache-2.0.


## Exploring Project Riff solution with CN Buildpacks

  1. [This issue](https://github.com/projectriff/riff/issues/654) indicates the intention to do deployment with [this Knative BuildTemplate for CN Buildpacks](https://github.com/knative/build-templates/blob/master/buildpacks/cnb.yaml). GitLab does not intend to use Build Templates, but the use of Build Templates **ARE NOT** a requirement to use CN Buildpacks.
  
  2. [As we see in the source code](https://github.com/knative/build-templates/blob/master/buildpacks/cnb.yaml#L44), This buildpack template accepts a **builder** as param. If not given, it will use the [CN Buildpack default one](https://github.com/knative/build-templates/blob/master/buildpacks/cnb.yaml#L15).
  To be able to use their own riff specific code and riff function invokers, riff implemented their custom CN BuildPack v3 compatible **builder**. A **builder** is an image with a compilation of several buildpacks. Buildpacks are a set of GO binaries that handles the Buildpack v3 API lifecycle: /detect /analyse /build /exporter.

  CN Buildpack CLI is used to create this **builder** image, E.g.:
  `pack create-builder -b builder-riff.toml projectriff/builder`

  Here is the [Project riff builder repo](https://github.com/projectriff/riff-buildpack-group/blob/master/builder-riff.toml)

  3. The above builder will have included the below buildpacks as seen on [builder-riff.toml](https://github.com/projectriff/riff-buildpack-group/blob/master/builder-riff.toml):
    - io.projectriff.riff (support to riff invoker buildpacks)
    - io.projectriff.java
    - io.projectriff.node
    - io.projectriff.command
    - ... many others

  4. Finally, each custom buildpack will have the function invoker as dependencies. So io.projectriff.node will define something like this:

   ```toml
    [[metadata.dependencies]]
    id      = "riff-invoker-node"
    name    = "riff Node Invoker"
    version = "0.1.0"
    uri     = "https://storage.googleapis.com/projectriff/node-function-invoker/releases/v0.1.0/node-function-invoker-0.1.0.tgz"
    sha256  = "8ca457e564574ead2f5e30c009e8124a6e7fe6a8ec17ef7b41ff40996c372dfd"
    stacks  = [ "io.buildpacks.stacks.bionic" ]
   ```

  5. The Function invokers will have different behaviors for calling the function depending on the language. E.g.:

- the [java-function-invoker](https://spring.io/projects/spring-boot) will use a [SpringBoot](https://spring.io/projects/spring-boot) application, which requires a function as a .jar file and uses a stream of data to communicate to the function.
- at runtime, the [node-function-invoker](https://github.com/projectriff/node-function-invoker) will `require()` the target function module. This module must export the function to invoke.

## Plans for future runtimes

Following Project riff approach, we could try creating: 

*  ruby-function-invoker 
*  ruby-buildpack 
*  GitLab buildpack v3 builder
*  Use our builder with `gitlabktl` to build and deploy our functions.

### What should we expect from implementing runtimes with Buildpacks API v3

right of the bat:

*  faster day-2 builds in comparison to normal Dockerfile builds, since we can automatically update OS base images and dependencies without having to rebuild all the image layers due to image rebase.
*  easier to handle project dependencies and native extension dependencies. Perhaps we could simple pass a `Gemfile` to a project and the buildpack could identify the native extensions that are necessary to be included in the image for the installation of these gems. This wasn't implemented by anyone yet. But I expect that during the port from buildpack API v2 to v3, this gets brought in.

in the long run:

*  community traction with the creation of many unified buildpacks ecosystems following the Buildpacks API v3 [platform-to-buildpack contract](https://github.com/buildpack/spec/blob/master/buildpack.md)
  
## Study on how to inject dependencies

### CN Buildpacks

##### Build Plan (as per CNF buildpacks specification)

CN Buildpack lifecycle will run the following golang binary artifacts in respective order: /detect /analyse /build /exporter. 

So, a Build Plan:

-  Is a TOML file with <plan> on top level
-  Iy may be passed to /bin/detect via stdin
-  Indicates dependencies like

```toml
  [<dependency name>]
  version = "<dependency version>"

  [<dependency name>.metadata]
  # buildpack-specific data
```

- /bin/detect may contribute to Build Plan by writing TOML to <plan>
- The final Build Plan is the fully-merged map that includes the contributions of the final /bin/detect, which could be a consolidated contribuition of several /bin/detect runs.
- Only when the /bin/build artifact runs is when the dependencies will be in fact installed in the image layer.

##### Dockerfile

Traditional way. Just copy a Gemfile into the image and run `bundle install`.

The problem with this approach is that we don't know when to include necessary native extensions. For instance, if one has the pg_gem in the Gemfile, the image might need to have libpq-dev installed as well.